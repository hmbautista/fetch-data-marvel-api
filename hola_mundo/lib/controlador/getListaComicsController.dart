import 'package:hola_mundo/persistencia/getListaComicsDAO.dart';
import 'package:hola_mundo/modelo/comics.dart';

/// Se define la clase GetListaComicsController para procesar
/// las operaciones generadas entre las entidades de la vista,
/// y las entidades del modelo.
class GetListaComicsController {
  Future<Comics> post;

  /// Se define una función para obtener
  /// el resultado de una llamada asincrona
  /// al API de Marvel, alojando el resultado
  /// en un objeto Future de la clase Comics
  Future<Comics> getComicsfromMarvelAPI() {
    /// Se hace una instancia de la clase GetListaComicsDAO
    /// para utilizar sus respectivas operaciones
    GetListaComicsDAO listaComicsDAO = GetListaComicsDAO();

    /// se asigna a una variable Future<Comics>
    /// el resultado de la llamada asincrona al API
    post = listaComicsDAO.getComicsfromMarvelAPI();

    /// se retorna la variable asignada previamente
    return post;
  }
}
