import 'package:hola_mundo/modelo/data.dart';

/// se define la clase Comics para contener los
/// datos y la información recibida de la llamada
/// asincrona a la solicitud del API
class Comics {
  /// se definen variables como atributos de la clase
  final int code;
  final String status;
  final Data data;

  /// se define el constructor de la clase
  Comics({this.code, this.status, this.data});

  /// se define la funcion para la serialización
  /// de la sección del archivo JSON recibido
  /// y seleccionar los valores que se necesitan
  /// en el Map<String, dynamic>
  factory Comics.fromJson(Map<String, dynamic> json) {
    ///Se retorna la información serializada
    return Comics(
        code: json['code'],
        status: json['status'],
        data: Data.fromJson(json['data']));
  }
}
