import 'package:hola_mundo/modelo/images.dart';
import 'package:hola_mundo/modelo/thumbnail.dart';

/// se define la clase Results para contener los
/// datos y la información recibida de la llamada
/// asincrona a la solicitud del API
class Results {
  /// se definen variables como atributos de la clase
  final int id;
  final String title;
  final String description;
  final Thumbnail thumbnail;

  /// Se define como atributo de la clase,
  /// una colección de elementos de la clase Images.
  List<Images> images;

  /// se define el constructor de la clase
  Results({this.id, this.title, this.description, this.images, this.thumbnail});

  /// se define la funcion para la serialización
  /// de la sección del archivo JSON recibido
  /// y seleccionar los valores que se necesitan
  /// en el Map<String, dynamic>
  factory Results.fromJson(Map<String, dynamic> json) {
    var list = json['images'] as List;
    List<Images> images = list.map((i) => Images.fromJson(i)).toList();

    /// se retorna la información serializada
    return Results(
        id: json['id'],
        title: json['title'],
        description: json['description'],
        images: images,
        thumbnail: Thumbnail.fromJson(json['thumbnail']));
  }
}
