/// se define la clase Images para contener los
/// datos y la información recibida de la llamada
/// asincrona a la solicitud del API
class Images {
  /// se definen variables como atributos de la clase
  final String path;
  final String extension;

  /// se define el constructor de la clase
  Images({this.path, this.extension});

  /// se define la funcion para la serialización
  /// de la sección del archivo JSON recibido
  /// y seleccionar los valores que se necesitan
  /// en el Map<String, dynamic>
  factory Images.fromJson(Map<String, dynamic> json) {
    /// se retorna la información serializada
    return Images(path: json['path'], extension: json['extension']);
  }
}
