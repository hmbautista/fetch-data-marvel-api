import 'package:hola_mundo/modelo/results.dart';

/// se define la clase Data para contener los
/// datos y la información recibida de la llamada
/// asincrona a la solicitud del API
class Data {
  /// se definen variables como atributos de la clase
  final int limit;
  final int total;
  final int count;

  /// Se define como atributo de la clase,
  /// una colección de elementos de la clase Results.
  List<Results> results;

  /// se define el constructor de la clase
  Data({this.limit, this.total, this.count, this.results});

  /// se define la funcion para la serialización
  /// de la sección del archivo JSON recibido
  /// y seleccionar los valores que se necesitan
  /// en el Map<String, dynamic>
  factory Data.fromJson(Map<String, dynamic> jsond) {
    /// En la serialización del JSON se define una lista
    /// de elementos para recibir la información del archivo
    var list = jsond['results'] as List;
    List<Results> results = list.map((i) => Results.fromJson(i)).toList();

    /// se retorna la información serializada
    return Data(
        limit: jsond['limit'],
        total: jsond['total'],
        count: jsond['count'],
        results: results);
  }
}
