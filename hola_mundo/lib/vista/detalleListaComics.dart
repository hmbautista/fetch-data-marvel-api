import 'package:flutter/material.dart';
import 'package:hola_mundo/modelo/results.dart';

/// Se define el widget DetalleListaComics para visualizar de forma
/// mas detallada la información del comic seleccionada por el usuario.
class DetalleListaComics extends StatelessWidget {
  final Results results;

  /// constructor de la clase en la cual se recibe como parametro
  /// la información del comic a visualizar mediante el uso de la
  /// estructura definida en la clase modelo/results
  DetalleListaComics({Key key, this.results}) : super(key: key);
  @override

  /// se definen los elementos que se ubicaran en el widget,
  /// junto las definiciones generales del mismo, tales como:
  /// titulo, temas, colores
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(   
         padding: EdgeInsets.all(5),
         alignment: Alignment.center,
        child: 
          new Card(
            color: Color.fromRGBO(238, 35, 19, 50),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            elevation: 5,
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.all(5),
                  alignment: Alignment.center,
                  /// para la visualización de la imagen del comic
                  /// se utiliza el widget Image.network para obtener
                  /// la imagen de una url de internet.
                  /// Se concatena la ruta de la imagen y la extension
                  /// de la misma.
                  child: new Image.network(
                    results.thumbnail.path + "." + results.thumbnail.extension,
                    fit: BoxFit.cover,
                  ),
                ),
                new Container(
                  //padding: EdgeInsets.all(5),
                  //alignment: Alignment.center,

                  /// Se define un widget de tipo Text para visualizar
                  /// el titulo del comic junto a su respectiva imagen.
                  child: new Text(results.title,
                      textAlign: TextAlign.justify,
                      overflow: TextOverflow.ellipsis,
                      style: new TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 3),
                ),
                new Expanded(                  
                  //padding: EdgeInsets.all(5),
                  //alignment: Alignment.topLeft,
                  /// Se define un widget de tipo Text para visualizar
                  /// la descripción del comic junto a su respectiva imagen.
                  child: new Text(results.description,                                    
                      textAlign: TextAlign.justify,                                      
                      overflow: TextOverflow.ellipsis,                      
                      maxLines: 15),
                ),
              ],
            ),
          ),      
      ),
    );
  }
}
