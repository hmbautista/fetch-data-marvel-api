import 'package:flutter/material.dart';
import 'package:hola_mundo/controlador/getListaComicsController.dart';
import 'package:hola_mundo/modelo/comics.dart';
import 'detalleListaComics.dart';

/// Se define el widget ListaComics para visualizar la lista de los comics
/// que retorna el API de Marvel
/// 
class ListaComics extends StatelessWidget {
  /// se define el constructor de la clase
  ListaComics({Key key}) : super(key: key);


  /// se definen los elementos que se ubicaran en el widget, 
  /// junto las definiciones generales del mismo, tales como:
  /// titulo, temas, colores
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Fetch Data Marvel API',
      theme: new ThemeData(
        primarySwatch: Colors.red,
      ),
      home: new Scaffold(
        body: new Container(          
          padding: EdgeInsets.all(10),
          /// Se define un FutureBuilder para procesar las solicitudes http get que se 
          /// reciben mediante el API especificado en este caso, el API de Marvel.
          /// Se utiliza principalmente para operaciones asincronas.
          child: new FutureBuilder<Comics>( 
            ///Se define la operación asincrona en la cual se obtendrá la información
            future: GetListaComicsController().getComicsfromMarvelAPI(),
            builder: (context, snapshot) {
              ///Si se reciben datos en la operación asincrona
              if (snapshot.hasData) {
                /// se procede a crear un ListViewBuilder
                return new ListView.builder(
                  /// se define el numero de elementos teniendo en cuenta la longitud 
                  /// de elementos del resultado
                    itemCount: snapshot.data.data.results.length,
                    /// se procede a crear los elementos para cada registro
                    /// del resultado de la operación asincrona
                    itemBuilder: (context, index) {
                      return new Column(                       
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            /// Se define un widget GestureDetector para capturar los eventos sobre los elementos
                            /// en el ListView.Builder
                            new GestureDetector(
                              /// para el evento de selección sobre el elemento de la lista
                              onTap: () {
                                /// se define un widget Navigator para realizar una redirección
                                /// al usuario luego de pulsar
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                        /// se hace una redirección al widget detalleListaComics para visualizar
                                        /// mas información sobre el comic seleccionado
                                            DetalleListaComics(
                                                results: snapshot.data.data
                                                    .results[index])));
                              },
                              /// para la visualización de los elementos del ListView 
                              /// se utiliza el widget Card
                              child: new Card(
                                color: Color.fromRGBO(238, 35, 19, 50),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                                elevation: 5,
                                child: new Column(
                                  children: <Widget>[
                                    new Row(
                                      children: <Widget>[
                                        new Container(
                                          padding: EdgeInsets.all(5),
                                          /// para la visualización de la miniatura del comic
                                          /// se utiliza el widget Image.network para obtener
                                          /// la imagen de una url de internet.
                                          /// Se concatena la ruta de la imagen y la extension
                                          /// de la misma.
                                          child: new Image.network(
                                            snapshot.data.data.results[index]
                                                    .thumbnail.path +
                                                "." +
                                                snapshot
                                                    .data
                                                    .data
                                                    .results[index]
                                                    .thumbnail
                                                    .extension,
                                            width: 70,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        new Expanded(
                                          /// Se define un widget de tipo Text para visualizar
                                          /// el texto del comic junto a su respectiva imagen.
                                          child: new Text(
                                              snapshot.data.data.results[index]
                                                  .title,
                                              textAlign: TextAlign.center,
                                              overflow: TextOverflow.ellipsis,
                                              style: new TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                   color: Color.fromRGBO(255, 255, 255, 100)),
                                              maxLines: 3),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ]);
                    });
                    /// si se presenta un error en el procesamiento
                    /// de la operación asincrona
              } else if (snapshot.hasError) {    
                /// se visualiza el texto de la información            
                return new Text("${snapshot.error}");
              }
              /// al arrancar el app y mientras se carga la información
              /// se mostrara un indicador de progreso circular
              return new Center(child: CircularProgressIndicator());
            },
          ),
        ),
      ),
    );
  }
}
