import 'package:hola_mundo/modelo/comics.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

/// Se define la clase GetListaComicsDAO para obtener la información
/// generada por el API de Marvel.
class GetListaComicsDAO {
  /// se definen las constantes con las llaves y valores
  /// utilizados en la generación de la llamada al API
  final String _apikey = '7c636fc9ee2be8abe1b1de0c22de1e4d';
  final String _ts = '9';
  final String _hash = '8d6fbcb70e21ab3f6739c567093ef0b5';

  /// Se define una funcion para la llamada asincrona
  /// a la API de Marvel.
  /// Future es una clase del core de Dart para trabajar
  /// con operaciones asincronas.
  Future<Comics> getComicsfromMarvelAPI() async {
    /// se asigna a una variable de tipo final,
    /// el resultado de la operación usando el metodo http.get
    final response = await http.get(
        'http://gateway.marvel.com/v1/public/comics?apikey=' +
            _apikey +
            '&ts=' +
            _ts +
            '&hash=' +
            _hash);

    if (response.statusCode == 200) {
      /// si al procesar la solicitud,
      /// se recibe una respuesta afirmativa,
      /// se procede a parsear el JSON utilizando
      /// las clases definidas en el modelo.
      return Comics.fromJson(json.decode(response.body));
    } else {
      /// si la respuesta no es afirmativa, se procede a mostrar 
      /// un mensaje de error.
      throw Exception('Failed to load post');
    }
  }
}
